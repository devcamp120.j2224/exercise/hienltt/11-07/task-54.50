import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.devcamp.Country;
import com.devcamp.Region;

public class App {
    public static void main(String[] args) throws Exception {        
        ArrayList<Country> countryList = new ArrayList<Country>();
        Country vietnam = new Country("Việt Nam", "VN", null);
        Country us = new Country("Hoa Kỳ", "US", null);
        Country russia = new Country("Nga", "RUS", null);
        countryList.add(vietnam);
        countryList.add(us);
        countryList.add(russia);
        // System.out.println(countryList);

        ArrayList<Region> regionList = new ArrayList<Region>();
        Region dongnai = new Region("Đồng Nai", 60);
        Region binhduong = new Region("Bình Dương", 61);
        Region tphcm = new Region("TP.HCM", 59);
        regionList.add(dongnai);
        regionList.add(binhduong);
        regionList.add(tphcm);

        String vietnamm = "Việt Nam";
        for(int i = 0; i < countryList.size(); i++){
            if (countryList.get(i).getCountryName() == vietnamm){
                System.out.println(regionList);
            } else {
                System.out.println("not found!!");
            }            
        }
    }
}
