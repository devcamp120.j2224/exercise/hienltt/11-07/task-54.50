package com.devcamp;

import java.util.ArrayList;

public class Country {
    String countryName;
    String countryCode;
    ArrayList<Region> regions;

    public Country(String countryName, String countryCode, ArrayList<Region> regions){
        this.countryName = countryName;
        this.countryCode = countryCode;
        this.regions = regions;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public void setRegions(ArrayList<Region> regions) {
        this.regions = regions;
    }

    public String getCountryName() {
        return countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public ArrayList<Region> getRegions() {
        return regions;
    }

    @Override
    public String toString(){
        return "[countryName = " + this.countryName + ", countryCode = " + this.countryCode 
        + ", regions = " + this.regions + "]";
    }
}
