package com.devcamp;

public class Region {
    String regionName;
    int regionCode;

    public Region(String regionName, int regionCode){
        this.regionName = regionName;
        this.regionCode = regionCode;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public int getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(int regionCode) {
        this.regionCode = regionCode;
    }

    @Override
    public String toString(){
        return "[regionName = " + this.regionName + ", regionCode = " + this.regionCode + "]";
    }
}
